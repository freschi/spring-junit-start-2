package at.spenger.junit.domain;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.omg.PortableInterceptor.SUCCESSFUL;
import org.springframework.format.datetime.joda.DateTimeParser;
import org.springframework.format.datetime.joda.LocalDateParser;

import at.spenger.junit.domain.Person.Sex;

public class ClubTest {
	private static DateTimeFormatter parser = DateTimeFormatter.ofPattern("dd-MM-yyyy").withZone(ZoneId.of("UTC"));

	@Test
	public void testEnter() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		assert club.getPersons().size() >= 1; 
	}

	@Test
	public void testNumberOf() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		assert club.numberOf() == 1;
	}
	
	@Test
	@Ignore
	public void testAverage() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		assert club.averageAge() == 44;
	}
	
	@Test
	public void testAverageStream() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		person = new Person("James", "Bond", parser.parse("01-01-1974", LocalDate::from), Sex.MALE);
		club.enter(person);
		assertEquals("Average Age Stream", 42, club.averageAgeStream(), 0.5);
	}
	
	@Test
	@Ignore
	public void testSort() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		club.enter(new Person("James", "Aond", parser.parse("02-01-1970", LocalDate::from), Sex.MALE));
		club.sort();
		assert club.getPersons().get(0).getLastName().equals("Aond");
		assert club.getPersons().get(1).getLastName().equals("Bond");
	}
	
	@Test
	public void testSortStream() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		club.enter(new Person("James", "Aond", parser.parse("02-01-1970", LocalDate::from), Sex.MALE));
		club.sortStream();
		assertEquals("Stream Sort Match", club.getPersons().get(0).getLastName(), "Aond");
		assertEquals("Stream Sort Match", club.getPersons().get(1).getLastName(), "Bond");
	}
	
	@Test
	public void testGroupByYear() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		person = new Person("James", "Bond", parser.parse("01-01-1974", LocalDate::from), Sex.MALE);
		club.enter(person);
		assertEquals("Stream group by year", 1970, club.orderByBirthYear().get(1970).get(0).getBirthday().getYear());
		assertEquals("Stream group by year", 1974, club.orderByBirthYear().get(1974).get(0).getBirthday().getYear());
	}
	
	@Test
	@Ignore
	public void testRemove() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		club.enter(new Person("James", "Aond", parser.parse("02-01-1970", LocalDate::from), Sex.MALE));
		assertEquals("2 Persons in club", 2, club.numberOf());
		club.leave(0);
		assert club.numberOf() == 1;
	}
	
	@Test
	public void testRemoveStream() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		club.enter(new Person("James", "Aond", parser.parse("02-01-1970", LocalDate::from), Sex.MALE));
		assertEquals("2 Persons in club", 2, club.numberOf());
		club.leave(person);
		assertEquals("1 Person in club", 1, club.numberOf());
	}
	
	@Test
	public void testDissolve() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		club.enter(new Person("James", "Aond", parser.parse("02-01-1970", LocalDate::from), Sex.MALE));
		assertEquals("2 Persons in club", 2, club.numberOf());
		club.dissolve();
		assert club.numberOf() == 0;
	}
	
	@Test
	public void testName() {
		Club club = new Club();
		club.setName("Hello World");
		assert club.getName().equals("Hello World");
		
	}
	
	@Test
	public void testSortBirthdays() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		club.enter(new Person("James", "Aond", parser.parse("02-01-1970", LocalDate::from), Sex.MALE));
		List<Person> persons = club.getOrderedBirthdays();
		assertEquals("Sort By Birthdays In Year", persons.get(0).getLastName(), "Bond");
		assertEquals("Sort By Birthdays In Year", persons.get(1).getLastName(), "Aond");
	}
	
	@Test
	public void testSeparateBySex() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		club.enter(new Person("Janine", "Aond", parser.parse("02-01-1970", LocalDate::from), Sex.FEMALE));
		club.sortStream();
		assertEquals("Separation by Sex Check", "Bond", club.getSeparatedBySex(Sex.MALE).get(0).getLastName());
		assertEquals("Separation by Sex Check", "Aond", club.getSeparatedBySex(Sex.FEMALE).get(0).getLastName());
	}

}
