package at.spenger.junit.domain;

import java.time.temporal.TemporalAmount;
import java.time.Duration;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.time.LocalDate;

import org.springframework.format.datetime.joda.LocalDateParser;

import at.spenger.junit.domain.Person.Sex;
import scala.annotation.meta.getter;

public class Club {
	private List<Person> l;
	private String name;
	
	public Club() {
		l = new ArrayList<>();
	}
	
	public boolean enter(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.add(p);
	}
	
	public int numberOf() {
		return l.size();
	}
	
	public List<Person> getPersons() {
		return Collections.unmodifiableList(l);
	}
	
	public double averageAge() {
		double total = 0;
		for(Person t : l) {
			Period period = Period.between(LocalDate.now(), t.getBirthday());
			total += period.getYears();
		}
		return total / numberOf();
	}
	
	public double averageAgeStream() {
		return l.stream().mapToDouble(Person::age).average().getAsDouble();
	}
	
	public void sort() {
		l.sort(new Comparator<Person>() {

			@Override
			public int compare(Person o1, Person o2) {
				int val;
				return (val = o1.getLastName().compareTo(o1.getLastName())) == 0 ? (o1.getFirstName().compareTo(o2.getFirstName())) : val;
			}
		});
	}
	
	public void sortStream() {
		l = l.stream()
				.sorted((p1, p2) -> (p1.getLastName().compareTo(
						p2.getLastName()) != 0 ? (p1.getLastName().compareTo(p2
						.getLastName())) : p1.getFirstName().compareTo(
						p2.getFirstName()))).collect(Collectors.toList());
	}
	
	public void leave(int index) {
		l.remove(index);
	}
	
	public void leave(Person p) {
		l = l.stream().filter(p1 -> p1.equals(p)).collect(Collectors.toList());
	}
	
	public Map<Integer, List<Person>> orderByBirthYear() {
		return l.stream().collect(Collectors.groupingBy((p) -> p.getBirthday().getYear()));
	}
	
	public void dissolve() {
		l.clear();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<Person> getOrderedBirthdays() {
		return l.stream()
				.sorted((p1, p2) -> p1.getBirthday().getMonthValue() != p2
						.getBirthday().getMonthValue() ? p1.getBirthday()
						.getMonthValue() - p2.getBirthday().getMonthValue()
						: p1.getBirthday().getDayOfMonth()
								- p2.getBirthday().getDayOfMonth())
				.collect(Collectors.toList());
	}
	
	public List<Person> getSeparatedBySex(Sex sex) {
		return l.stream().filter(p -> p.getSex() == sex).collect(Collectors.toList());
	}
	

}
